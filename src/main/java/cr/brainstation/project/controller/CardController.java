package cr.brainstation.project.controller;

import cr.brainstation.project.core.service.CardService;
import cr.brainstation.project.model.Card;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import cr.brainstation.project.support.response.ErrorResponse;
import cr.brainstation.project.support.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class CardController {

    @Autowired
    private CardService cardService;

    @PostMapping("/card")
    public ResponseEntity createCard(@RequestAttribute(name = "userId") String userId,
                                        @RequestBody Card card) {
        try {
            Card newCard = this.cardService.createCard(userId, card);
            return new ResponseEntity<>(new SuccessResponse(newCard), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/card")
    public ResponseEntity getCards(@RequestAttribute(name = "userId") String userId) {
        try {
            List<Card> cards = this.cardService.getCards(userId);
            return new ResponseEntity<>(new SuccessResponse(cards), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/card/{cardId}")
    public ResponseEntity updateCard(@RequestAttribute(name = "userId") String userId,
                                        @PathVariable(name = "cardId") String cardId,
                                        @RequestBody Card card) {
        try {
            Card updatedCard = this.cardService.updateCard(userId, cardId, card);
            return new ResponseEntity<>(new SuccessResponse(updatedCard), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/card/{cardId}")
    public ResponseEntity getCard(@PathVariable(name = "cardId") String cardId) {
        try {
            Card card = this.cardService.getCard(cardId);
            return new ResponseEntity<>(new SuccessResponse(card), HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/card/{cardId}")
    public ResponseEntity deleteCard(@PathVariable(name = "cardId") String cardId) {
        try {
            this.cardService.deleteCard(cardId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }
    
}
