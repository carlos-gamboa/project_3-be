package cr.brainstation.project.controller;

import cr.brainstation.project.core.service.ProductService;
import cr.brainstation.project.model.Accessory;
import cr.brainstation.project.model.Console;
import cr.brainstation.project.model.Product;
import cr.brainstation.project.model.VideoGame;
import cr.brainstation.project.model.enums.Brand;
import cr.brainstation.project.model.enums.ProductCategory;
import cr.brainstation.project.support.response.ErrorResponse;
import cr.brainstation.project.support.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import org.springframework.data.domain.Pageable;

@CrossOrigin
@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/product")
    public ResponseEntity getAllProducts(Pageable pageable,
                                         @RequestParam(name = "category") String category,
                                         @RequestParam(name = "search") String search,
                                         @RequestParam(name = "brand") String brand) {
        try {
            Page<Product> products = this.productService.getProducts(pageable, category, search, brand);
            return new ResponseEntity<>(new SuccessResponse(products), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/product/game")
    public ResponseEntity getAllGames(Pageable pageable) {
        try {
            Page<Product> games = this.productService.getProductsByCategory(pageable, "videogame");
            return new ResponseEntity<>(new SuccessResponse(games), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/product/console")
    public ResponseEntity getAllConsoles(Pageable pageable) {
        try {
            Page<Product> consoles = this.productService.getProductsByCategory(pageable, "console");
            return new ResponseEntity<>(new SuccessResponse(consoles), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/product/accessory")
    public ResponseEntity getAllAccessories(Pageable pageable) {
        try {
            Page<Product> accessories = this.productService.getProductsByCategory(pageable, "accessory");
            return new ResponseEntity<>(new SuccessResponse(accessories), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/product/game/{id}")
    public ResponseEntity getGame(@PathVariable(name = "id") String id) {
        try {
            VideoGame game = this.productService.getVideoGame(id);
            return new ResponseEntity<>(new SuccessResponse(game), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/product/console/{id}")
    public ResponseEntity getConsole(@PathVariable(name = "id") String id) {
        try {
            Console console = this.productService.getConsole(id);
            return new ResponseEntity<>(new SuccessResponse(console), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/product/accessory/{id}")
    public ResponseEntity getAccessory(@PathVariable(name = "id") String id) {
        try {
            Accessory accessory = this.productService.getAccessory(id);
            return new ResponseEntity<>(new SuccessResponse(accessory), HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }
}
