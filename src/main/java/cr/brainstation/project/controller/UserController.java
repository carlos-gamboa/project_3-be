package cr.brainstation.project.controller;

import cr.brainstation.project.core.service.UserService;
import cr.brainstation.project.model.Login;
import cr.brainstation.project.model.User;
import cr.brainstation.project.support.exception.EmailAlreadyInUseException;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FailedLoginException;
import cr.brainstation.project.support.response.ErrorResponse;
import cr.brainstation.project.support.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/user")
    public ResponseEntity createUser(@RequestBody User user) {
        try {
            User newUser = this.userService.createUser(user);
            return new ResponseEntity<>(new SuccessResponse(newUser), HttpStatus.OK);
        }
        catch (EmailAlreadyInUseException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.CONFLICT);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/user/login")
    public ResponseEntity login(@RequestBody User user) {
        try {
            Login login = this.userService.login(user);
            return new ResponseEntity<>(new SuccessResponse(login), HttpStatus.OK);
        }
        catch (FailedLoginException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/user/renew")
    public ResponseEntity getUserByToken(@RequestAttribute(name = "userId") String id) {
        try {
            Login login = this.userService.renew(id);
            return new ResponseEntity<>(new SuccessResponse(login), HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

}
