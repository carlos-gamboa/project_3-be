package cr.brainstation.project.controller;

import cr.brainstation.project.core.service.BillingService;
import cr.brainstation.project.model.Billing;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import cr.brainstation.project.support.response.ErrorResponse;
import cr.brainstation.project.support.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class BillingController {

    @Autowired
    private BillingService billingService;

    @PostMapping("/billing")
    public ResponseEntity createBilling(@RequestAttribute(name = "userId") String userId,
                                     @RequestBody Billing billing) {
        try {
            Billing newBilling = this.billingService.createBilling(userId, billing);
            return new ResponseEntity<>(new SuccessResponse(newBilling), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/billing")
    public ResponseEntity getBillings(@RequestAttribute(name = "userId") String userId) {
        try {
            List<Billing> billings = this.billingService.getBillings(userId);
            return new ResponseEntity<>(new SuccessResponse(billings), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/billing/{billingId}")
    public ResponseEntity updateBilling(@RequestAttribute(name = "userId") String userId,
                                        @PathVariable(name = "billingId") String billingId,
                                        @RequestBody Billing billing) {
        try {
            Billing updatedBilling = this.billingService.updateBilling(userId, billingId, billing);
            return new ResponseEntity<>(new SuccessResponse(updatedBilling), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/billing/{billingId}")
    public ResponseEntity getBilling(@PathVariable(name = "billingId") String billingId) {
        try {
            Billing billing = this.billingService.getBilling(billingId);
            return new ResponseEntity<>(new SuccessResponse(billing), HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/billing/{billingId}")
    public ResponseEntity deleteBilling(@PathVariable(name = "billingId") String billingId) {
        try {
            this.billingService.deleteBilling(billingId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

}
