package cr.brainstation.project.controller;

import cr.brainstation.project.core.service.ShoppingCartService;
import cr.brainstation.project.model.Order;
import cr.brainstation.project.model.ShoppingCartItem;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import cr.brainstation.project.support.exception.NotEnoughProductException;
import cr.brainstation.project.support.response.ErrorResponse;
import cr.brainstation.project.support.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @PostMapping("/shopping-cart")
    public ResponseEntity addToShoppingCart(@RequestAttribute(name = "userId") String userId,
                                            @RequestBody ShoppingCartItem shoppingCartItem) {
        try {
            ShoppingCartItem newShoppingCartItem = this.shoppingCartService.addToShoppingCart(shoppingCartItem, userId);
            return new ResponseEntity<>(new SuccessResponse(newShoppingCartItem), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/shopping-cart")
    public ResponseEntity getShoppingCart(@RequestAttribute(name = "userId") String userId) {
        try {
            List<ShoppingCartItem> shoppingCart = this.shoppingCartService.getShoppingCart(userId);
            return new ResponseEntity<>(new SuccessResponse(shoppingCart), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @PutMapping("/shopping-cart/{shoppingCartId}")
    public ResponseEntity updateCard(@RequestAttribute(name = "userId") String userId,
                                     @PathVariable(name = "shoppingCartId") String shoppingCartItemId,
                                     @RequestBody ShoppingCartItem shoppingCartItem) {
        try {
            ShoppingCartItem updatedShoppingCartItem = this.shoppingCartService.updateShoppingCartItem(shoppingCartItemId, shoppingCartItem, userId);
            return new ResponseEntity<>(new SuccessResponse(updatedShoppingCartItem), HttpStatus.OK);
        }
        catch (FieldRequiredException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @DeleteMapping("/shopping-cart/{shoppingCartId}")
    public ResponseEntity deleteCard(@RequestAttribute(name = "userId") String userId,
                                     @PathVariable(name = "shoppingCartId") String shoppingCartItemId) {
        try {
            this.shoppingCartService.removeFromShoppingCart(shoppingCartItemId, userId);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @PostMapping("/shopping-cart/checkout")
    public ResponseEntity checkoutFromShoppingCart(@RequestAttribute(name = "userId") String userId,
                                            @RequestBody Order order) {
        try {
            Order newOrder = this.shoppingCartService.checkoutFromShoppingCart(order, userId);
            newOrder = this.shoppingCartService.getOrder(newOrder.getId());
            return new ResponseEntity<>(new SuccessResponse(newOrder), HttpStatus.OK);
        }
        catch (NotEnoughProductException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/order")
    public ResponseEntity getOrders(@RequestAttribute(name = "userId") String userId) {
        try {
            List<Order> orders = this.shoppingCartService.getOrders(userId);
            return new ResponseEntity<>(new SuccessResponse(orders), HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/order/{orderId}")
    public ResponseEntity getOrder(@PathVariable(name = "orderId") String orderId) {
        try {
            Order newOrder = this.shoppingCartService.getOrder(new Long(orderId));
            return new ResponseEntity<>(new SuccessResponse(newOrder), HttpStatus.OK);
        }
        catch (EntityNotFoundException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.NOT_FOUND);
        }
        catch (Exception e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.UNAUTHORIZED);
        }
    }
}
