package cr.brainstation.project.support.jwt;

import cr.brainstation.project.model.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Service
public class JWTHelper {
    private static String apiKey = "rIvFc_OJPJhpNcTKDAti6uuiP8bNUuEepxLUesg_jDrZnqqHAwIsuAEKk9zBiSELpK4mHKqPFrWYmmVjYrox8uP4kehCm2TlTdGgtb-eAXifkgZjYsMhfpIciSiO83b9xf-mjlFyQyhgCr-6Fy6Rg4HvmnsfMW7NJ-SbAfIw6SOHTf4IwYvkJO02XfIjwhCvnENnReH9rKbh64bKjnp1s3fZbgZuMts7T-k7mEjCPqry-WNIDISm-tdT2PPCh3y_YhChDpv096DVJlb0C10Iw069Ug5DeWWmivw_YfTSbZmaS2IYKMEaxUTCscsFet_rJHQ8dEvSGQUxn00Xn1wl3eU";

    public String generateToken(User user) {
        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(apiKey);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        String jwt = Jwts.builder()
                .setSubject(user.getId().toString())
                .setExpiration(this.generateExpirationDate())
                .claim("username", user.getUsername())
                .signWith(signingKey)
                .compact();

        return jwt;
    }

    public Claims parseJWT(String jwt) {
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(apiKey))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }

    private Date generateExpirationDate() {
        LocalDateTime expirationDate = LocalDateTime.now();
        expirationDate = expirationDate.plusDays(1);
        return Date.from(expirationDate.atZone(ZoneId.systemDefault()).toInstant());
    }
}
