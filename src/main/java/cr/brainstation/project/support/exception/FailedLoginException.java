package cr.brainstation.project.support.exception;

public class FailedLoginException extends RuntimeException {
    public FailedLoginException() {
        super("The email and password don't match.");
    }
}
