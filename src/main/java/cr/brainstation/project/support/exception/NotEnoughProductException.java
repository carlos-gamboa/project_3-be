package cr.brainstation.project.support.exception;

public class NotEnoughProductException extends RuntimeException {

    public NotEnoughProductException() {
    }

    public NotEnoughProductException(String productName) {
        super("There are not enough " + productName + " in stock to place your order.");
    }

}
