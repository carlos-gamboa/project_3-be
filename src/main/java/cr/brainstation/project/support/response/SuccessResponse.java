package cr.brainstation.project.support.response;

import org.springframework.data.domain.Page;

public class SuccessResponse {
    private Boolean error;
    private String message;
    private Object data;
    private PaginationInfo pagination;

    public SuccessResponse(Object data) {
        if (data instanceof Page) {
            this.data = ((Page) data).getContent();
            this.pagination = new PaginationInfo((Page) data);
        } else {
            this.data = data;
        }

        this.error = false;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PaginationInfo getPagination() {
        return pagination;
    }

    public void setPagination(PaginationInfo pagination) {
        this.pagination = pagination;
    }
}
