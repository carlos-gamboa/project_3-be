package cr.brainstation.project.support.response;

import org.springframework.data.domain.Page;

public class PaginationInfo {
    private int pageNumber;
    private int totalPages;

    public PaginationInfo(Page page) {
        this.pageNumber = page.getNumber();
        this.totalPages = page.getTotalPages();
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
