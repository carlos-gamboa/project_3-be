package cr.brainstation.project.core.service.impl;

import cr.brainstation.project.core.dao.CardDAO;
import cr.brainstation.project.core.dto.CardDTO;
import cr.brainstation.project.core.dto.UserDTO;
import cr.brainstation.project.core.service.CardService;
import cr.brainstation.project.core.service.UserService;
import cr.brainstation.project.model.Card;
import cr.brainstation.project.model.User;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    private CardDAO cardDAO;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public List<Card> getCards(String userId) throws RuntimeException {
        User user;

        try {
            user = this.userService.getUser(userId);
        } catch (Exception e) {
            throw e;
        }

        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());

        return this.cardDAO.findAllByUser(userDTO).stream()
                .map(Card::new)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Card createCard(String userId, Card card) throws RuntimeException {
        User user;

        try {
            user = this.userService.getUser(userId);
        } catch (Exception e) {
            throw e;
        }

        if (card.getCardNumber() == null) {
            throw new FieldRequiredException("Card Number");
        }
        if (card.getCardName() == null) {
            throw new FieldRequiredException("Card Name");
        }
        if (card.getExpirationDate() == null) {
            throw new FieldRequiredException("Expiration Date");
        }

        CardDTO newCard = new CardDTO(card, user);

        newCard = this.cardDAO.save(newCard);

        return new Card(newCard);
    }

    @Override
    @Transactional
    public Card updateCard(String userId, String cardId, Card card) throws RuntimeException {
        User user;

        try {
            user = this.userService.getUser(userId);
        } catch (Exception e) {
            throw e;
        }

        if (card.getCardNumber() == null) {
            throw new FieldRequiredException("Card Number");
        }
        if (card.getCardName() == null) {
            throw new FieldRequiredException("Card Name");
        }
        if (card.getExpirationDate() == null) {
            throw new FieldRequiredException("Expiration Date");
        }

        CardDTO newCard = this.cardDAO.findById(Long.parseLong(cardId)).orElse(null);
        if (newCard == null){
            throw new EntityNotFoundException("card", "id");
        }

        newCard = new CardDTO(card, user);
        newCard.setId(Long.parseLong(cardId));
        newCard = this.cardDAO.save(newCard);

        return new Card(newCard);
    }

    @Override
    @Transactional
    public Card getCard(String cardId) throws RuntimeException {
        CardDTO card = this.cardDAO.findById(Long.parseLong(cardId)).orElse(null);
        if (card == null){
            throw new EntityNotFoundException("card", "id");
        }

        return new Card(card);
    }

    @Override
    public void deleteCard(String cardId) throws RuntimeException {
        CardDTO card = this.cardDAO.findById(Long.parseLong(cardId)).orElse(null);
        if (card == null){
            throw new EntityNotFoundException("card", "id");
        }

        this.cardDAO.delete(card);
    }
}
