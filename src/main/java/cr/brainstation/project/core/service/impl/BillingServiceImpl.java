package cr.brainstation.project.core.service.impl;

import cr.brainstation.project.core.dao.BillingDAO;
import cr.brainstation.project.core.dto.BillingDTO;
import cr.brainstation.project.core.dto.UserDTO;
import cr.brainstation.project.core.service.BillingService;
import cr.brainstation.project.core.service.UserService;
import cr.brainstation.project.model.Billing;
import cr.brainstation.project.model.User;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BillingServiceImpl implements BillingService {

    @Autowired
    private BillingDAO billingDAO;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public List<Billing> getBillings(String userId) throws RuntimeException {
        User user;

        try {
            user = this.userService.getUser(userId);
        } catch (Exception e) {
            throw e;
        }

        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());

        return this.billingDAO.findAllByUserAndIsActive(userDTO, true).stream()
                .map(Billing::new)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public Billing createBilling(String userId, Billing billing) throws RuntimeException {
        User user;

        try {
            user = this.userService.getUser(userId);
        } catch (Exception e) {
            throw e;
        }

        if (billing.getFirstName() == null) {
            throw new FieldRequiredException("First Name");
        }
        if (billing.getMobile() == null) {
            throw new FieldRequiredException("Mobile");
        }
        if (billing.getAddress() == null) {
            throw new FieldRequiredException("Address");
        }
        if (billing.getCity() == null) {
            throw new FieldRequiredException("City");
        }
        if (billing.getCounty() == null) {
            throw new FieldRequiredException("County");
        }
        if (billing.getPostCode() == null) {
            throw new FieldRequiredException("Post Code");
        }

        BillingDTO newBilling = new BillingDTO(billing, user);

        newBilling = this.billingDAO.save(newBilling);

        return new Billing(newBilling);
    }

    @Override
    @Transactional
    public Billing updateBilling(String userId, String billingId, Billing billing) throws RuntimeException {
        User user;

        try {
            user = this.userService.getUser(userId);
        } catch (Exception e) {
            throw e;
        }

        if (billing.getFirstName() == null) {
            throw new FieldRequiredException("First Name");
        }
        if (billing.getMobile() == null) {
            throw new FieldRequiredException("Mobile");
        }
        if (billing.getAddress() == null) {
            throw new FieldRequiredException("Address");
        }
        if (billing.getCity() == null) {
            throw new FieldRequiredException("City");
        }
        if (billing.getCounty() == null) {
            throw new FieldRequiredException("County");
        }
        if (billing.getPostCode() == null) {
            throw new FieldRequiredException("Post Code");
        }

        BillingDTO newBilling = this.billingDAO.findById(Long.parseLong(billingId)).orElse(null);
        if (newBilling == null){
            throw new EntityNotFoundException("billing", "id");
        }

        newBilling = new BillingDTO(billing, user);
        newBilling.setId(Long.parseLong(billingId));
        newBilling = this.billingDAO.save(newBilling);

        return new Billing(newBilling);
    }

    @Override
    @Transactional
    public Billing getBilling(String billingId) throws RuntimeException {
        BillingDTO billing = this.billingDAO.findById(Long.parseLong(billingId)).orElse(null);
        if (billing == null){
            throw new EntityNotFoundException("billing", "id");
        }

        return new Billing(billing);
    }

    @Override
    @Transactional
    public void deleteBilling(String billingId) throws RuntimeException {
        BillingDTO billing = this.billingDAO.findById(Long.parseLong(billingId)).orElse(null);
        if (billing == null){
            throw new EntityNotFoundException("billing", "id");
        }

        billing.setActive(false);

        this.billingDAO.save(billing);
    }
}
