package cr.brainstation.project.core.service;

import cr.brainstation.project.model.Login;
import cr.brainstation.project.model.User;

public interface UserService {

    User createUser(User user) throws RuntimeException;

    User getUser(String id) throws RuntimeException;

    Login login(User user) throws RuntimeException;

    Login renew(String id) throws RuntimeException;

}
