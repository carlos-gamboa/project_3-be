package cr.brainstation.project.core.service.impl;

import cr.brainstation.project.core.dao.ReviewDAO;
import cr.brainstation.project.core.dao.UserDAO;
import cr.brainstation.project.core.dto.ReviewDTO;
import cr.brainstation.project.core.dto.UserDTO;
import cr.brainstation.project.core.service.ReviewService;
import cr.brainstation.project.model.Review;
import cr.brainstation.project.model.User;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import cr.brainstation.project.support.exception.FieldRequiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

@Service
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewDAO reviewDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    @Transactional
    public Review addReview(String userId, Review review) throws RuntimeException {
        UserDTO user;

        user = this.userDAO.findById(Long.parseLong(userId)).orElse(null);

        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }

        if (review.getTitle() == null) {
            throw new FieldRequiredException("Title");
        }
        if (review.getComment() == null) {
            throw new FieldRequiredException("Comment");
        }
        if (review.getRating() == null) {
            throw new FieldRequiredException("Rating");
        }

        ReviewDTO newReview = new ReviewDTO(review);
        newReview.setUser(user);

        newReview = this.reviewDAO.save(newReview);

        return new Review(newReview);
    }

    @Override
    @Transactional
    public Review updateReview(String userId, String reviewId, Review review) throws RuntimeException {
        UserDTO user;

        user = this.userDAO.findById(Long.parseLong(userId)).orElse(null);

        if (user == null) {
            throw new EntityNotFoundException("user", "id");
        }

        if (review.getTitle() == null) {
            throw new FieldRequiredException("Title");
        }
        if (review.getComment() == null) {
            throw new FieldRequiredException("Comment");
        }
        if (review.getRating() == null) {
            throw new FieldRequiredException("Rating");
        }

        ReviewDTO reviewToUpdate = this.reviewDAO.findById(Long.parseLong(reviewId)).orElse(null);
        if (reviewToUpdate == null){
            throw new EntityNotFoundException("review", "id");
        }

        reviewToUpdate = new ReviewDTO(review);
        reviewToUpdate.setUser(user);
        reviewToUpdate.setId(Long.parseLong(reviewId));
        reviewToUpdate = this.reviewDAO.save(reviewToUpdate);

        return new Review(reviewToUpdate);
    }

    @Override
    @Transactional
    public Review getReview(String reviewId) throws RuntimeException {
        ReviewDTO review = this.reviewDAO.findById(Long.parseLong(reviewId)).orElse(null);
        if (review == null){
            throw new EntityNotFoundException("billing", "id");
        }

        return new Review(review);
    }

    @Override
    @Transactional
    public void deleteReview(String reviewId) throws RuntimeException {
        ReviewDTO review = this.reviewDAO.findById(Long.parseLong(reviewId)).orElse(null);
        if (review == null){
            throw new EntityNotFoundException("billing", "id");
        }

        this.reviewDAO.delete(review);
    }
}
