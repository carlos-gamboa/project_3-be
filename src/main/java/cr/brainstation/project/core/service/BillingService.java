package cr.brainstation.project.core.service;

import cr.brainstation.project.model.Billing;

import java.util.List;

public interface BillingService {

    List<Billing> getBillings(String userId) throws RuntimeException;

    Billing createBilling(String userId, Billing billing) throws RuntimeException;

    Billing updateBilling(String userId, String billingId, Billing billing) throws RuntimeException;

    Billing getBilling(String billingId) throws RuntimeException;

    void deleteBilling(String billingId) throws RuntimeException;

}
