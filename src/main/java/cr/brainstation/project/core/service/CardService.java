package cr.brainstation.project.core.service;

import cr.brainstation.project.model.Card;

import java.util.List;

public interface CardService {

    List<Card> getCards(String userId) throws RuntimeException;

    Card createCard(String userId, Card card) throws RuntimeException;

    Card updateCard(String userId, String cardId, Card card) throws RuntimeException;

    Card getCard(String cardId) throws RuntimeException;

    void deleteCard(String cardId) throws RuntimeException;
    
}
