package cr.brainstation.project.core.service;

import cr.brainstation.project.model.Accessory;
import cr.brainstation.project.model.Console;
import cr.brainstation.project.model.Product;
import cr.brainstation.project.model.VideoGame;
import cr.brainstation.project.model.enums.Brand;
import cr.brainstation.project.model.enums.ProductCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ProductService {

    Page<Product> getProducts(Pageable pageable,
                              String category,
                              String search,
                              String brand) throws RuntimeException;

    Page<Product> getProductsByCategory(Pageable pageable, String category) throws RuntimeException;

    VideoGame getVideoGame(String id) throws RuntimeException;

    Accessory getAccessory(String id) throws RuntimeException;

    Console getConsole(String id) throws RuntimeException;

}
