package cr.brainstation.project.core.service.impl;

import cr.brainstation.project.core.dao.AccessoryDAO;
import cr.brainstation.project.core.dao.ConsoleDAO;
import cr.brainstation.project.core.dao.ProductDAO;
import cr.brainstation.project.core.dao.VideoGameDAO;
import cr.brainstation.project.core.dto.AccessoryDTO;
import cr.brainstation.project.core.dto.ConsoleDTO;
import cr.brainstation.project.core.dto.VideoGameDTO;
import cr.brainstation.project.core.service.ProductService;
import cr.brainstation.project.model.*;
import cr.brainstation.project.model.enums.Brand;
import cr.brainstation.project.model.enums.ProductCategory;
import cr.brainstation.project.support.exception.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;

import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private VideoGameDAO videoGameDAO;

    @Autowired
    private ConsoleDAO consoleDAO;

    @Autowired
    private AccessoryDAO accessoryDAO;

    @Override
    @Transactional
    public Page<Product> getProducts(Pageable pageable,
                                     String category,
                                     String search,
                                     String brand) throws RuntimeException {
        Page<Product> products;
        Brand brandSearch;
        ProductCategory categorySearch;
        String nameSearch = "";
        if (search != null) {
            nameSearch = search;
        }
        System.out.println(brand);
        System.out.println(category);
        if (category.length() == 0) {
            categorySearch = ProductCategory.NONE;
        }
        else {
            categorySearch = ProductCategory.valueOf(category.toUpperCase());
        }
        if (brand.length() == 0) {
            brandSearch = Brand.NONE;
        }
        else {
            brandSearch = Brand.valueOf(brand.toUpperCase());
        }
        if (brandSearch != Brand.NONE && categorySearch != ProductCategory.NONE) {
            products = this.productDAO.findAllByCategoryAndBrandAndNameIgnoreCaseContaining(
                    pageable, categorySearch, brandSearch, nameSearch).map(Product::new);
        }
        else if (brandSearch != Brand.NONE) {
            products = this.productDAO.findAllByBrandAndNameIgnoreCaseContaining(
                    pageable, brandSearch, nameSearch).map(Product::new);
        }
        else if (categorySearch != ProductCategory.NONE){
            products = this.productDAO.findAllByCategoryAndNameIgnoreCaseContaining(
                    pageable, categorySearch, nameSearch).map(Product::new);
        }
        else {
            products = this.productDAO.findAllByNameIgnoreCaseContaining(
                    pageable, nameSearch).map(Product::new);
        }
        return products;
    }

    @Override
    @Transactional
    public Page<Product> getProductsByCategory(Pageable pageable, String category) throws RuntimeException {
        ProductCategory cat;
        if (category.equals("videogame")) {
            cat = ProductCategory.VIDEO_GAME;
        }
        else if (category.equals("console")) {
            cat = ProductCategory.CONSOLE;
        }
        else {
            cat = ProductCategory.ACCESSORY;
        }
        return this.productDAO.findAllByCategory(pageable, cat).map(Product::new);
    }

    @Override
    @Transactional
    public VideoGame getVideoGame(String id) throws RuntimeException {
        VideoGameDTO videoGameDTO = this.videoGameDAO.findById(Long.parseLong(id)).orElse(null);
        if (videoGameDTO == null) {
            throw new EntityNotFoundException("video game", "id");
        }
        VideoGame videoGame = new VideoGame(videoGameDTO);
        videoGame.setReviews(videoGameDTO.getReviews().stream().map(Review::new).collect(Collectors.toList()));
        return videoGame;
    }

    @Override
    @Transactional
    public Accessory getAccessory(String id) throws RuntimeException {
        AccessoryDTO accessoryDTO = this.accessoryDAO.findById(Long.parseLong(id)).orElse(null);
        if (accessoryDTO == null) {
            throw new EntityNotFoundException("accessory", "id");
        }
        Accessory accessory = new Accessory(accessoryDTO);
        accessory.setReviews(accessoryDTO.getReviews().stream().map(Review::new).collect(Collectors.toList()));
        return accessory;
    }

    @Override
    @Transactional
    public Console getConsole(String id) throws RuntimeException {
        ConsoleDTO consoleDTO = this.consoleDAO.findById(Long.parseLong(id)).orElse(null);
        if (consoleDTO == null) {
            throw new EntityNotFoundException("console", "id");
        }
        Console console = new Console(consoleDTO);
        console.setReviews(consoleDTO.getReviews().stream().map(Review::new).collect(Collectors.toList()));
        return console;
    }
}
