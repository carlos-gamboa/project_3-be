package cr.brainstation.project.core.service;

import cr.brainstation.project.model.Order;
import cr.brainstation.project.model.ShoppingCartItem;

import java.util.List;

public interface ShoppingCartService {

    ShoppingCartItem addToShoppingCart(ShoppingCartItem shoppingCart, String userId);

    ShoppingCartItem updateShoppingCartItem(String shoppingCartId, ShoppingCartItem shoppingCart, String userId);

    List<ShoppingCartItem> getShoppingCart(String userId);

    void removeFromShoppingCart(String shoppingCartId, String userId);

    Order checkoutFromShoppingCart(Order order, String userId);

    List<Order> getOrders(String userId);

    Order getOrder(Long orderId);
}
