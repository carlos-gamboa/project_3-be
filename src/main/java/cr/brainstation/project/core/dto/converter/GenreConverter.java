package cr.brainstation.project.core.dto.converter;

import cr.brainstation.project.model.enums.Genre;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GenreConverter implements AttributeConverter<Genre, String> {

    @Override
    public String convertToDatabaseColumn(Genre attribute) {
        switch (attribute) {
            case ACTION:
                return "action";
            case ADVENTURE:
                return "adventure";
            case ROLEPLAY:
                return "roleplay";
            case SIMULATION:
                return "simulation";
            case STRATEGY:
                return "strategy";
            case SPORTS:
                return "sports";
            default:
                return "other";
        }
    }

    @Override
    public Genre convertToEntityAttribute(String dbData) {
        switch (dbData) {
            case "action":
                return Genre.ACTION;
            case "adventure":
                return Genre.ADVENTURE;
            case "roleplay":
                return Genre.ROLEPLAY;
            case "simulation":
                return Genre.SIMULATION;
            case "strategy":
                return Genre.STRATEGY;
            case "sports":
                return Genre.SPORTS;
            default:
                return Genre.OTHER;
        }
    }
}
