package cr.brainstation.project.core.dto;

import cr.brainstation.project.model.Order;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

/**
 * Class that represent's an User's orders in the application.
 */
@Entity
@Table(name = "orders")
public class OrderDTO extends BaseEntity {

    /**
     * Date when the order is set to arrive.
     */
    @Column(name = "arriving_date", nullable = false)
    private LocalDate arrivingDate;

    /**
     * List of items that were ordered.
     */
    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER)
    private Set<ShoppingCartItemDTO> shoppingCart;

    /**
     * Billing Address where the order will be sent.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="billing_id", nullable=false)
    private BillingDTO billing;


    public OrderDTO() {
    }

    public OrderDTO(Order order) {
        this.arrivingDate = LocalDate.now().plusDays(2);
        this.billing = new BillingDTO(order.getBilling());
    }

    public Set<ShoppingCartItemDTO> getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(Set<ShoppingCartItemDTO> shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public LocalDate getArrivingDate() {
        return arrivingDate;
    }

    public void setArrivingDate(LocalDate arrivingDate) {
        this.arrivingDate = arrivingDate;
    }

    public BillingDTO getBilling() {
        return billing;
    }

    public void setBilling(BillingDTO billing) {
        this.billing = billing;
    }
}

