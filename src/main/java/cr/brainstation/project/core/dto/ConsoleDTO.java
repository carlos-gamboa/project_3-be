package cr.brainstation.project.core.dto;

import javax.persistence.*;
import java.util.Set;

/**
 * Class that represents a Console in the application.
 */
@Entity
@Table(name = "consoles")
@PrimaryKeyJoinColumn(name = "productId")
public class ConsoleDTO extends ProductDTO {

    /**
     * Set of the accessories that work with the Console.
     */
    @OneToMany(mappedBy="console", fetch = FetchType.LAZY)
    private Set<AccessoryDTO> accessories;

    /**
     * Set of Video Games for the Console.
     */
    @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    @JoinTable(
            name = "console_video_games",
            joinColumns = { @JoinColumn(name = "console_id") },
            inverseJoinColumns = { @JoinColumn(name = "video_game_id") }
    )
    private Set<VideoGameDTO> videoGames;

    public ConsoleDTO() {
    }

    public Set<AccessoryDTO> getAccessories() {
        return accessories;
    }

    public void setAccessories(Set<AccessoryDTO> accessories) {
        this.accessories = accessories;
    }

    public Set<VideoGameDTO> getVideoGames() {
        return videoGames;
    }

    public void setVideoGames(Set<VideoGameDTO> videoGames) {
        this.videoGames = videoGames;
    }
}
