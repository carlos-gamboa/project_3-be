package cr.brainstation.project.core.dto;

import cr.brainstation.project.model.User;

import javax.persistence.*;
import java.util.Set;

/**
 * Class that represents a user within the application, a user has a login and
 * password and all the information inherited from the
 * {@link PersonDTO} class.
 *
 */
@Entity
@Table(name = "users")
public class UserDTO extends PersonDTO {

    /**
     * Username, it can't be empty, null and can't have duplicates. It will be
     * the user's email address.
     */
    @Column(name = "username", unique = true)
    private String username;

    /**
     * Password (will be encrypted).
     */
    @Column(name = "password", nullable = false)
    private String password;

    @OneToMany(mappedBy="user", fetch = FetchType.LAZY)
    private Set<BillingDTO> billingBook;

    @OneToMany(mappedBy="user", fetch = FetchType.LAZY)
    private Set<CardDTO> cards;

    @OneToMany(mappedBy="user", fetch = FetchType.EAGER)
    private Set<ShoppingCartItemDTO> shoppingCart;

    @OneToMany(mappedBy="user", fetch = FetchType.LAZY)
    private Set<ReviewDTO> reviews;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this.setId(user.getId());
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<BillingDTO> getBillingBook() {
        return billingBook;
    }

    public void setBillingBook(Set<BillingDTO> billingBook) {
        this.billingBook = billingBook;
    }

    public Set<CardDTO> getCards() {
        return cards;
    }

    public void setCards(Set<CardDTO> cards) {
        this.cards = cards;
    }

    public Set<ShoppingCartItemDTO> getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(Set<ShoppingCartItemDTO> shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Set<ReviewDTO> getReviews() {
        return reviews;
    }

    public void setReviews(Set<ReviewDTO> reviews) {
        this.reviews = reviews;
    }
}
