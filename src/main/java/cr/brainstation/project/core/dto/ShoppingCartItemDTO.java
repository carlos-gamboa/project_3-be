package cr.brainstation.project.core.dto;

import cr.brainstation.project.model.ShoppingCartItem;

import javax.persistence.*;

/**
 * Class that represent's an User's shopping cart in the application.
 */
@Entity
@Table(name = "shopping_cart_item")
public class ShoppingCartItemDTO extends BaseEntity {

    /**
     * Quantity of the Product in the ShoppingCart.
     */
    @Column(name = "quantity", nullable = false, columnDefinition = "int default 1")
    private Integer quantity;

    /**
     * User that owns this Product.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id", nullable=false)
    private UserDTO user;

    /**
     * Product owned by the user.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="product_id", nullable=false)
    private ProductDTO product;

    /**
     * Order where the item was purchased.
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="order_id", nullable=true)
    private OrderDTO order;

    public ShoppingCartItemDTO() {
    }

    public ShoppingCartItemDTO(ShoppingCartItem shoppingCartItem) {
        this.id = shoppingCartItem.getId();
        this.quantity = shoppingCartItem.getQuantity();
        this.product = new ProductDTO(shoppingCartItem.getProduct());
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public OrderDTO getOrder() {
        return order;
    }

    public void setOrder(OrderDTO order) {
        this.order = order;
    }
}
