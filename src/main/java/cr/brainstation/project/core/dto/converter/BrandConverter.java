package cr.brainstation.project.core.dto.converter;

import cr.brainstation.project.model.enums.Brand;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class BrandConverter implements AttributeConverter<Brand, String> {

    @Override
    public String convertToDatabaseColumn(Brand attribute) {
        switch (attribute) {
            case NINTENDO:
                return "nintendo";
            case PLAYSTATION:
                return "playstation";
            case XBOX:
                return "xbox";
            default:
                return "";
        }
    }

    @Override
    public Brand convertToEntityAttribute(String dbData) {
        switch (dbData) {
            case "nintendo":
                return Brand.NINTENDO;
            case "playstation":
                return Brand.PLAYSTATION;
            case "xbox":
                return Brand.XBOX;
            default:
                return Brand.NONE;
        }
    }
}

