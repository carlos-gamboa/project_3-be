package cr.brainstation.project.core.dto;

import cr.brainstation.project.core.dto.converter.GenreConverter;
import cr.brainstation.project.model.enums.Genre;

import javax.persistence.*;
import java.util.Set;

/**
 * Class that represents a Video Game in the application.
 */
@Entity
@Table(name = "video_games")
@PrimaryKeyJoinColumn(name = "productId")
public class VideoGameDTO extends ProductDTO {

    /**
     * Synopsis.
     */
    @Column(name = "synopsis", columnDefinition = "TEXT")
    private String synopsis;

    /**
     * Developer.
     */
    @Column(name = "developer")
    private String developer;

    /**
     * Publisher.
     */
    @Column(name = "publisher")
    private String publisher;

    /**
     * Genre.
     */
    @Column(name = "genre")
    @Convert(converter = GenreConverter.class)
    private Genre genre;

    /**
     * Set with the Consoles this Video Game works with.
     */
    @ManyToMany(mappedBy = "videoGames", fetch = FetchType.LAZY)
    private Set<ConsoleDTO> consoles;

    public VideoGameDTO() {
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Set<ConsoleDTO> getConsoles() {
        return consoles;
    }

    public void setConsoles(Set<ConsoleDTO> consoles) {
        this.consoles = consoles;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}
