package cr.brainstation.project.core.dto.converter;

import cr.brainstation.project.model.enums.Gender;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender, String> {

    @Override
    public String convertToDatabaseColumn(Gender attribute) {
        switch (attribute) {
            case MALE:
                return "male";
            case FEMALE:
                return "female";
            default:
                return "other";
        }
    }

    @Override
    public Gender convertToEntityAttribute(String dbData) {
        switch (dbData) {
            case "male":
                return Gender.MALE;
            case "female":
                return Gender.FEMALE;
            default:
                return Gender.OTHER;
        }
    }
}
