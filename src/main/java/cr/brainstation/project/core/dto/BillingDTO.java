package cr.brainstation.project.core.dto;

import cr.brainstation.project.model.Billing;
import cr.brainstation.project.model.User;

import javax.persistence.*;
import java.util.Set;

/**
 * Class that represents a Billing Information in the application
 */
@Entity
@Table(name = "billings")
public class BillingDTO extends BaseEntity {
    /**
     * First Name of the person the bill is addressed to.
     */
    @Column(name = "first_name", nullable = false)
    private String firstName;

    /**
     * Last Name of the person the bill is addressed to.
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * Phone number of the person the bill is addressed to.
     */
    @Column(name = "mobile", nullable = false)
    private String mobile;

    /**
     * Address of the bill.
     */
    @Column(name = "address", nullable = false)
    private String address;

    /**
     * City the bill is addressed to.
     */
    @Column(name = "city", nullable = false)
    private String city;

    /**
     * County the bill is addressed to.
     */
    @Column(name = "county", nullable = false)
    private String county;

    /**
     * Post code is addressed to.
     */
    @Column(name = "post_code", nullable = false)
    private String postCode;

    /**
     * If the billing address is active
     */
    @Column(name = "is_active")
    private Boolean isActive;

    /**
     * User that owns the Billing Information.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id", nullable=false)
    private UserDTO user;

    @OneToMany(mappedBy="billing", fetch = FetchType.LAZY)
    private Set<OrderDTO> orders;

    public BillingDTO() {
    }

    public BillingDTO(Billing billing) {
        this.id = billing.getId();
    }

    public BillingDTO(Billing billing, User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());

        this.id = billing.getId();
        this.firstName = billing.getFirstName();
        this.lastName = billing.getLastName();
        this.mobile = billing.getMobile();
        this.address = billing.getAddress();
        this.city = billing.getCity();
        this.county = billing.getCounty();
        this.postCode = billing.getPostCode();
        this.isActive = true;
        this.setUser(userDTO);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public UserDTO getUser() {
        return user;
    }

    public Set<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(Set<OrderDTO> orders) {
        this.orders = orders;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
