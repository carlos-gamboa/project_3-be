package cr.brainstation.project.core.dto;

import cr.brainstation.project.model.Review;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "reviews")
public class ReviewDTO extends BaseEntity {

    /**
     * General description of the product's review.
     */
    @Column(name = "title", nullable = false)
    private String title;

    /**
     * The product's review.
     */
    @Column(name = "comment", nullable = false, columnDefinition = "TEXT")
    private String comment;

    /**
     * Product's rating.
     */
    @Column(name = "rating", nullable = false)
    private Float rating;

    /**
     * Review's likes.
     */
    @Column(name = "likes", columnDefinition = "int default 0")
    private Integer likes;

    /**
     * Review's dislikes.
     */
    @Column(name = "dislikes", columnDefinition = "int default 0")
    private Integer dislikes;

    /**
     * Review's dislikes.
     */
    @Column(name = "publication_date")
    private LocalDateTime publicationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id", nullable=false)
    private UserDTO user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="product_id", nullable=false)
    private ProductDTO product;

    public ReviewDTO() {
    }

    public ReviewDTO(Review review) {
        this.title = review.getTitle();
        this.comment = review.getComment();
        this.rating = review.getRating();
        this.publicationDate = LocalDateTime.now();
        this.product = new ProductDTO(review.getProduct());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }

    public LocalDateTime getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(LocalDateTime publicationDate) {
        this.publicationDate = publicationDate;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }
}
