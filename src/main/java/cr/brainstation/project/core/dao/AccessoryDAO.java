package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.AccessoryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessoryDAO extends JpaRepository<AccessoryDTO, Long> {
}