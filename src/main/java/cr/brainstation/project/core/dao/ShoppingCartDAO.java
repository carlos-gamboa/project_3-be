package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.ProductDTO;
import cr.brainstation.project.core.dto.ShoppingCartItemDTO;
import cr.brainstation.project.core.dto.UserDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ShoppingCartDAO extends JpaRepository<ShoppingCartItemDTO, Long> {
    Set<ShoppingCartItemDTO> findAllByUserAndOrderIsNull(UserDTO user);

    ShoppingCartItemDTO findByProductAndUser(ProductDTO product, UserDTO user);
}
