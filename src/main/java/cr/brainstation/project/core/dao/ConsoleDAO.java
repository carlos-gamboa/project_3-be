package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.ConsoleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsoleDAO extends JpaRepository<ConsoleDTO, Long> {
}
