package cr.brainstation.project.core.dao;

import cr.brainstation.project.core.dto.CardDTO;
import cr.brainstation.project.core.dto.UserDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface CardDAO extends JpaRepository<CardDTO, Long> {
    Set<CardDTO> findAllByUser(UserDTO user);
}