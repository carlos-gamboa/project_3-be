package cr.brainstation.project.config;

import cr.brainstation.project.support.filter.JWTFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@ComponentScan("cr.brainstation.project")
public class AppConfig {

    @Bean
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

    @Bean
    public FilterRegistrationBean <JWTFilter> filterRegistrationBean() {
        FilterRegistrationBean<JWTFilter> registrationBean = new FilterRegistrationBean<>();
        JWTFilter JWTFilter = new JWTFilter();

        registrationBean.setFilter(JWTFilter);
        registrationBean.addUrlPatterns("/user/renew/*");
        registrationBean.addUrlPatterns("/billing/*");
        registrationBean.addUrlPatterns("/card/*");
        registrationBean.addUrlPatterns("/shopping-cart/*");
        registrationBean.addUrlPatterns("/order/*");
        registrationBean.addUrlPatterns("/review/*");
        registrationBean.setOrder(1);
        return registrationBean;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
