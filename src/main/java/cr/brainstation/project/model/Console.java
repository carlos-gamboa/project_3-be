package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.ConsoleDTO;

public class Console extends Product{

    public Console() {
    }

    public Console(ConsoleDTO consoleDTO) {
        super(consoleDTO);
    }
}
