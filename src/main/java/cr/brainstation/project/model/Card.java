package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.CardDTO;

import java.time.LocalDate;

public class Card {

    private Long id;

    private String cardName;

    private String cardNumber;

    private String expirationDate;

    public Card() {
    }

    public Card(CardDTO cardDTO) {
        this.id = cardDTO.getId();
        this.cardName = cardDTO.getCardName();
        this.cardNumber = cardDTO.getCardNumber();
        this.expirationDate = cardDTO.getExpirationDate();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
}
