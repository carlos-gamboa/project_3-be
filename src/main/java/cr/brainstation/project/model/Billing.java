package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.BillingDTO;

public class Billing {

    private Long id;

    private String firstName;

    private String lastName;

    private String mobile;

    private String address;

    private String city;

    private String county;

    private String postCode;

    public Billing() {
    }

    public Billing(BillingDTO billingDTO) {
        this.id = billingDTO.getId();
        this.firstName = billingDTO.getFirstName();
        this.lastName = billingDTO.getLastName();
        this.mobile = billingDTO.getMobile();
        this.address = billingDTO.getAddress();
        this.city = billingDTO.getCity();
        this.county = billingDTO.getCounty();
        this.postCode = billingDTO.getPostCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

}
