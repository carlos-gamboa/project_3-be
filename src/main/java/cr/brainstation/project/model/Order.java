package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.OrderDTO;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class Order {

    private Long id;

    private LocalDate arrivingDate;

    private List<ShoppingCartItem> shoppingCart;

    private Billing billing;

    public Order() {
    }

    public Order(OrderDTO orderDTO) {
        this.id = orderDTO.getId();
        this.arrivingDate = orderDTO.getArrivingDate();
        this.shoppingCart = orderDTO.getShoppingCart().stream().map(ShoppingCartItem::new).collect(Collectors.toList());
        this.billing = new Billing(orderDTO.getBilling());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getArrivingDate() {
        return arrivingDate;
    }

    public void setArrivingDate(LocalDate arrivingDate) {
        this.arrivingDate = arrivingDate;
    }

    public List<ShoppingCartItem> getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(List<ShoppingCartItem> shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public Billing getBilling() {
        return billing;
    }

    public void setBilling(Billing billing) {
        this.billing = billing;
    }
}
