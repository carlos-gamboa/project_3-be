package cr.brainstation.project.model;

import cr.brainstation.project.core.dto.UserDTO;

public class User extends Person {

    private String username;

    private String password;

    public User() {}

    public User(UserDTO user) {
        super(user.getId(), user.getFirstName(), user.getLastName(), user.getDateOfBirth(),
                user.getGender(), user.getProfilePictureLink());
        this.username = user.getUsername();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
