package cr.brainstation.project.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Gender {
    MALE("male"),
    FEMALE("female"),
    OTHER("other");

    private String value;

    Gender(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() { return this.value; }

    @JsonCreator
    public static Gender create(String val) {
        Gender[] genders = Gender.values();
        for (Gender gender : genders) {
            if (gender.getValue().equals(val)) {
                return gender;
            }
        }
        return OTHER;
    }
}
