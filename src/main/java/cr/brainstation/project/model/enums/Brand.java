package cr.brainstation.project.model.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Brand {
    NINTENDO("nintendo"),
    PLAYSTATION("playstation"),
    XBOX("xbox"),
    NONE("");

    private String value;

    Brand(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() { return this.value; }

    @JsonCreator
    public static Brand create(String val) {
        Brand[] brands = Brand.values();
        for (Brand brand : brands) {
            if (brand.getValue().equals(val)) {
                return brand;
            }
        }
        return NONE;
    }
}
