package cr.brainstation.project.model;

import cr.brainstation.project.model.enums.Gender;

import java.time.LocalDate;

public class Person {

    protected Long id;

    protected String firstName;

    protected String lastName;

    protected LocalDate dateOfBirth;

    protected Gender gender;

    protected String profilePictureLink;

    public Person() {
    }

    public Person(Long id, String firstName, String lastName, LocalDate dateOfBirth, Gender gender, String profilePictureLink) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.profilePictureLink = profilePictureLink;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getProfilePictureLink() {
        return profilePictureLink;
    }

    public void setProfilePictureLink(String profilePictureLink) {
        this.profilePictureLink = profilePictureLink;
    }
}
